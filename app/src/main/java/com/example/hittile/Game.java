package com.example.hittile;


import android.nfc.FormatException;

/**
 * Class which contains the board algorithmic side, the number of stroke and the id of the grid.
 * Each manipulation of the grid is done by methods of this class.
 * @author Charron M.
 * @version 2.0
 * License : GPL or later
 **/
public class Game {

    //the board of boolean, true if the cell is activated else false.
    private boolean[][] board;

    //the number of stroke
    private int stroke;

    //the size of the board
    private int size;

    //boolean which testifies if the game is finished or not
    private boolean game_finished;

    //the id of the grid, its composed of the size as the first number then the starting state of
    //the grid
    private String id;

    /**
     * Constructor of the class which required the size of the board to init
     * @param size The size of the board
     */
    public Game (int size) {
        this.size = size;
        //all cells are initialized to false by default
        this.board = new boolean[this.size][this.size];
        this.stroke = 0;
        this.game_finished = false;
        //randomize the board to have some cell activated at the beginning of the game and get the
        //id
        this.randomizeBoard();
    }

    /**
     * Method which check for all cell of the board if they are all true. If no cell has false
     * like value, the attribute game_finished is set to true.
     */
    public void checkWin () {
        boolean then = true;
        int row = 0;

        //browse the board
        while ( row < this.size && then ) {
            int column = 0;
            while ( column < this.size && then ) {
                //if the cell is false
                if ( !this.board[row][column] ) {
                    then = false;
                }
                column++;
            }
            row++;
        }
        //if all cell is true, so the game is finished
        if ( then ) {
            this.game_finished = true;
        }
    }

    /**
     * Method to activate some button to avoid a board with all button inactive and generate the id.
     * A button has 2 chances on 5 to be activated at the beginning. To get the id, the first figure
     * of the string is the size of the board. Then its the decimal value of the binary number
     * generate by the cells of the board. The last cell of the board represent the bit 0, if this
     * cell is activate then the bit 0 takes the value 1 else 0. So each cells are browsed, all in
     * a row from right to left before moving to the previous column and each cell has a bit. As
     * many bits as there are cells.
     */
    public void randomizeBoard () {
        String binary = "";
        //browse each cell, from above left to bottom right (row after row)
        for ( int x = 0 ; x < this.size ; x++ ) {
            for (int y = 0 ; y < this.size ; y++ ) {
                //generate a random number between 0 and 4
                int alea = (int) Math.round(Math.random()*5);
                if ( alea == 1 || alea == 0 || alea == 2 ) {
                    //the string id gets the value 0 for the next bit (decreasing bit)
                    binary += "0";
                    this.board[x][y] = false;
                } else {
                    //the string id gets the value 1 for the next bit (decreasing bit)
                    binary += "1";
                    this.board[x][y] = true;
                }
            }
        }
        try {
            //create the id
            this.id = this.size + "" + Long.parseLong(binary, 2);

        }
        catch(Exception e){
            //if these instructions are executed, the conversion did not work because the number is
            //outside the limit of the integer representation in java
            this.initBoard();
            this.randomizeBoard();
        }
    }

    /**
     * Set all the cells of the grid to false, to inactive button.
     */
    public void initBoard () {
        for ( int i = 0 ; i < this.size ; i++ ) {
            for ( int j = 0 ; j< this.size ; j++ ) {
                this.board[i][j] = false;
            }
        }
    }

    /**
     * Set the value of the cells according the id passed in parameters and resize the board if is
     * necessary
     * @param id The id of the board to set
     */
    public void setGridById ( String id ) {
        //get the size by getting the first character of the string
        this.size = Integer.parseInt(id.charAt(0)+"");
        //create a new board
        this.board = new boolean[this.size][this.size];

        //get the binary number which represent the state of starting state of the board
        id = Long.toBinaryString(Long.parseLong(id.substring(1)));
        //get the number of bit
        int index = id.length()+1;
        //add a 0 to the last bits that don't have value (lost with converting to decimal number)
        while ( index <= this.size*this.size ) {
            id = "0" + id;
            index ++;
        }
        index = 0;
        //for each bit, set the value of the cell which represent this bit with its value
        for ( int i = 0 ; i < this.size ; i++ ) {
            for ( int j = 0 ; j < this.size ; j++ ) {
                if ( (id.charAt(index)+"").equals("0") ) {
                    this.board[i][j] = false;
                } else {
                    this.board[i][j] = true;
                }
                index++;
            }
        }
    }

    //getters and setters of this class
    public int getStroke () {return this.stroke;}
    public void setStroke (int stroke) {this.stroke = stroke;}
    public String getId () {return this.id;}
    public void incrementStroke() {this.stroke++;}
    public boolean isFinish() {return this.game_finished;}
    public void setFinish(boolean bool) {this.game_finished = bool;}
    public boolean getCell (int row, int column){return this.board[row][column];}
    public void setCell (int row, int column, boolean value){this.board[row][column] = value;}
    public boolean[][] getBoard () {return this.board;}
    public int getSize () {return this.size;}
    public void switchCell (int row, int column) {
        //if the coordonates are not above the size limits of the board
        if (row >= 0 && row < this.board.length && column >= 0 && column < this.board[row].length) {
            this.board[row][column] = !this.board[row][column];
        }
    }
    

    /**
     * Method to display the board in string format. Cross is display if a cell is true, a dot
     * if false.
     * @return The board in a string format
     */
    public String toSring () {
        String ret = "";
        for ( int row = 0 ; row < this.size ; row ++ ) {
            for ( int column = 0 ; column < this.size ; column ++ ) {

                //if cell is true
                if ( board[row][column] ) {
                    ret += " X";

                //if cell is false
                }else{
                    ret += " .";
                }
            }
            ret += "\n";
        }
        return ret;
    }
}
