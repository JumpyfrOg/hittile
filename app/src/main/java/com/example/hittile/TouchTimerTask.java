package com.example.hittile;

import java.util.TimerTask;

/**
 * Class which be run when the user make a touch on a cell, whose row, column and dimention of the 
 * cell touched are specified at the construction of the object.
 * @author Charron M.
 * @version 2.0
 * License : GPL or later
 **/
public class TouchTimerTask extends TimerTask {

    //the activity which run this task
    GameActivity game;

    //the row of the cell concerned
    int row;

    //the column of the cell concerned
    int column;

    //the horizontal position of the click (position on a row)
    int xTouch;

    //the vertical position of the click (position on a column)
    int yTouch;

    //the width of the cell touched
    int width;

    //the height of the cell touched
    int height;

    /**
     * Constructor of the class which requires coordinate of the cell concerned, the dimension of
     * the cell and the activity where is launched. This parameters are saved like attributes
     * of this class.
     * @param row The row of the cell
     * @param column The column of the cell
     * @param game The activity where is launched the task
     * @param xTouch The horizontal coordinates 
     * @param yTouch The vertical coordinates
     * @param width The width of the cell touched
     * @param height The height of the cell touched
     */
    public TouchTimerTask(int row, int column, GameActivity game, int xTouch, int yTouch, int width, int heigth) {
        this.row = row;
        this.column = column;
        this.game = game;
        this.xTouch = xTouch;
        this.yTouch = yTouch;
        this.width = width;
        this.height = height;
    }

    @Override
    /**
     * Method executed when the task is launched, when the user has touch a cell for more than
     * 0.5 seconds. It set the isTouch mode to true, and call the reactive method for a touch.
     */
    public void run() {
        game.isTouch = true;
        game.cellTouched(this.row, this.column, this.xTouch, this.yTouch, this.width, this.height);
    }
}
