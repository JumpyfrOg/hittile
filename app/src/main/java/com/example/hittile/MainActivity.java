package com.example.hittile;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;

/**
 * Activity which represent the menu where you can choose a size for the board. The version of the
 * app and the license are show on the screen.
 * @author Charron M.
 * @version 2.0
 * License : GPL or later
 **/
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //set the text of the version of the app
        TextView t = findViewById(R.id.version_label);
        t.setText("Version : "+getResources().getString(R.string.app_version));
        //set the license of the app
        t = findViewById(R.id.license_label);
        t.setText("License : "+getResources().getString(R.string.license_name));

        //add a listener to the new game button to launch a game
        ImageButton but = findViewById(R.id.button_new_game);
        but.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent m) {

                //if the user remove their finger from the button
                if ( m.getAction() == MotionEvent.ACTION_UP ) {
                    //set the active button image to the button
                    ImageButton but = (ImageButton) v;
                    but.setImageResource(R.drawable.button_new);

                    //get the boolean of the choices to know which mode is chosen
                    RadioButton sq_6 = findViewById(R.id.button6x6);
                    RadioButton sq_7 = findViewById(R.id.button7x7);
                    RadioButton sq_8 = findViewById(R.id.button8x8);
                    boolean square_6 = sq_6.isChecked();
                    boolean square_7 = sq_7.isChecked();
                    boolean square_8 = sq_8.isChecked();

                    //create a new intent to prepare switching activity
                    Intent intent = new Intent(MainActivity.this, GameActivity.class);

                    //put a flag to specify this activity is closed and go back to the previous
                    //which is the main activity of the app
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("square_6", square_6);
                    intent.putExtra("square_7", square_7);
                    intent.putExtra("square_8", square_8);

                    //start activity
                    startActivity(intent);

                //if the user put a finger on the button
                }else if ( m.getAction() == MotionEvent.ACTION_DOWN ) {
                    //set the inactive button image to the button
                    ImageButton but = (ImageButton) v;
                    but.setImageResource(R.drawable.button_new_active);
                }
            return true;
            }
            

        });


    }
}
