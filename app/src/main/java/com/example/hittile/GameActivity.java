package com.example.hittile;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Timer;

/**
 * The activity of the game screen with the board displayed on the sreen and where the user play.
 * The screen contains too 2 button, one to reset the board and one to go back to the menu. And the
 * id of the board and the number of stroke are also displayed.
 * @author Charron M.
 * @version 2.0
 * License : GPL or later
 **/
public class GameActivity extends AppCompatActivity {

    //the game which contain the board
    Game partie;

    //the timer which launch a task after a time
    Timer timer;

    //boolean to know if a task is scheduled by the timer or not
    boolean timer_scheduled;

    //the timer task to execute to react at a touch
    TouchTimerTask timer_task;

    //boolean to know if the click on a cell is a simple click, or a touch (keep click)
    public boolean isTouch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_layout);

        //set the grid on the view
        ViewStub v = findViewById(R.id.inclued_grid);
        //get the size passed through the activities
        Bundle bundle = getIntent().getExtras();
        //size to 6 by default
        int size = 6;
        //according the size choosed by the user, put the layout of cell corresponding to this one.
        if ( bundle.containsKey("square_6") && bundle.containsKey("square_7") && bundle.containsKey("square_8") ) {
            if (bundle.getBoolean("square_6")) {
                v.setLayoutResource(R.layout.grid_layout_6);
            } else if (bundle.getBoolean("square_7")) {
                v.setLayoutResource(R.layout.grid_layout_7);
                size = 7;
            } else if (bundle.getBoolean("square_8")) {
                v.setLayoutResource(R.layout.grid_layout_8);
                size = 8;
            }
        }
        View inflated = v.inflate();

        //initialize some attributes
        this.partie = new Game(size);
        this.isTouch = false;
        this.timer_scheduled = false;

        //init menu button listener
        this.setMenuListener();

        //set the image to initialize the board
        this.refrechCells();

        //init the listeners of the cells of the board
        this.initGridListener();

        //init reset button listener
        this.initResetListener();

        //set the label to display the number of stroke an board id
        this.setStrokeLabel();

    }


    /**
     * Method to set a touch listener for each cells of the cells returned by call
     * the method getCells()
     */
    private void initGridListener () {
        View[][] grid = this.getCells();

        for ( int i = 0 ; i < this.partie.getSize() ; i++ ) {
            for ( int j = 0 ; j < this.partie.getSize() ; j++ ) {

                final int row = i;
                final int column = j;

                //add to the cell a touch listener
                grid[row][column].setOnTouchListener(new View.OnTouchListener(){
                    public boolean onTouch(View v, MotionEvent m) {

                        //get the coordinates of the click (pixel of the button, not the view)
                        int x = (int) m.getX();
                        int y = (int) m.getY();

                        //method to executed to can call getWidth and getHeight corectly
                        v.measure(0,0);
                        //normally size is 140, 140
                        int width = v.getWidth();
                        int height = v.getHeight();

                        //if user put their finger on the cell
                        if ( m.getAction() == MotionEvent.ACTION_DOWN ) {
                            //launch timer to know if is just a click or not
                            initTimer(row,column, x, y, width, height);
                        }

                        //if user remove their finger from the cell
                        if ( m.getAction() == MotionEvent.ACTION_UP ) {
                            //stop timer because the finger is removed
                            stopTimer();
                        }

                        //if the action which that calls this method is placing or removing a finger from a cell
                        if ( m.getAction() == MotionEvent.ACTION_DOWN || m.getAction() == MotionEvent.ACTION_UP ) {
                            //if the last action, so the action in progress is a touch
                            if (isTouch) {
                                //then the timer task has already call the reactive method, so remove isTouch mode
                                isTouch = false;
                                //else, is just a click
                            }else{
                                //if any timer is scheduled (else the user hasn't remove his finger)
                                if ( !timer_scheduled ) {
                                    //call the reactive methove for a simple click
                                    cellPlayed(row, column,x,y,width,height);
                                    //check if the game is finished
                                    checkPartie();
                                }
                            }
                        }


                        //default return value to run correctly this method
                        return true;
                    }
                });
            }
        }
    }

    /**
     * Method which check if the game is finished by call checkWin(). If the game continue, nothing
     * is do, otherwise a popup announcing the game won is displayed. This activity is started when
     * the pop-up closes.
     */
    public void checkPartie () {
        //check if game is won
        this.partie.checkWin();

        //if game is won
        if ( this.partie.isFinish() ) {

            //set the status game to no finished (for the next game)
            this.partie.setFinish(false);

            //make a popup and show it.
            try {
                Looper.prepare();
            }
            catch(Exception e){}
            new AlertDialog.Builder(this)
                    .setTitle("Félicitation")
                    .setMessage("Vous avez gagné la partie.")
                    //when "OK" button is clicked
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //launch a new activity, the menu
                            Intent intent = new Intent(GameActivity.this,MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    })
                    .show();
            Looper.loop();
        }
    }


    /**
     * Init the listener of the button reset to reset the game.
     */
    public void initResetListener () {
        ImageView but = findViewById(R.id.reset_button);
        but.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent m) {
                //if the user remove their finger from the screen
                if ( m.getAction() == MotionEvent.ACTION_UP ) {
                    //set the inactive restart button image
                    ImageView but = (ImageView) v;
                    but.setImageResource(R.drawable.button_recommencer);
                    partie.setGridById(partie.getId());
                    //set game not finish
                    partie.setFinish(false);
                    partie.setStroke(0);
                    //refresh the cells and the stroke label
                    refrechCells();
                    setStrokeLabel();

                //if the user put a finger on the button
                } else if ( m.getAction() == MotionEvent.ACTION_DOWN ) {
                    //set the active restart button image
                    ImageView but = (ImageView) v;
                    but.setImageResource(R.drawable.button_recommencer_active);
                }
                return true;
            }
        });
    }

    /**
     * Method to set a listener to the menu button to go back to the menu
     */
    public void setMenuListener () {
        ImageView menu = (ImageView) findViewById(R.id.menu_button);
        menu.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent m) {

                //if user remove their finger from the screen
                if (m.getAction() == MotionEvent.ACTION_UP) {
                    ImageView but = (ImageView) v;
                    //set inactive menu button image
                    but.setImageResource(R.drawable.button_menu);
                    //launch a new activity, menu activity
                    Intent intent = new Intent(GameActivity.this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                //if user put a finger on the button
                } else if (m.getAction() == MotionEvent.ACTION_DOWN) {
                    //set active menu button image
                    ImageView but = (ImageView) v;
                    but.setImageResource(R.drawable.button_menu_active);
                }
                return true;
            }

        });
    }


    /**
     * Method executed when user make a click. Switches the value and the image of the played cell,
     * according the coordinates passed by parameters, and the cells near (according the
     * coordinates of the click).
     * @param row The row of the cell played
     * @param column The column of the cell played
     * @param xTouch The horizontal coordinates 
     * @param yTouch The vertical coordinates
     * @param width The width of the cell touched
     * @param height The height of the cell touched
     */
    public void cellPlayed ( int row, int column, int xTouch, int yTouch, int width, int height ) {
        //increment stroke and display the new numbers of stroke
        this.partie.incrementStroke();
        this.setStrokeLabel();

        //to debug 
        //this.debugTouchZone(xTouch,yTouch,width,height);

        //the cell touched
        this.partie.switchCell(row,column);
        this.refreshOneCell(row,column);

        //cells switch according to the position of the click on the button (button divided into
        //9 equal square areas)
        if ( isAboveLeft(xTouch,yTouch,width,height) ) {
            this.partie.switchCell(row-1,column-1);
            this.refreshOneCell(row-1,column-1);
            this.partie.switchCell(row,column-1);
            this.refreshOneCell(row,column-1);
            this.partie.switchCell(row-1,column);
            this.refreshOneCell(row-1,column);

        } else if ( isAbove(xTouch,yTouch,width,height) ) {
            this.partie.switchCell(row-1,column);
            this.refreshOneCell(row-1,column);
            this.partie.switchCell(row-1,column-1);
            this.refreshOneCell(row-1,column-1);
            this.partie.switchCell(row,column-1);
            this.refreshOneCell(row,column-1);
            this.partie.switchCell(row-1,column+1);
            this.refreshOneCell(row-1,column+1);
            this.partie.switchCell(row,column+1);
            this.refreshOneCell(row,column+1);

        } else if ( isAboveRight(xTouch,yTouch,width,height) ) {
            this.partie.switchCell(row-1,column+1);
            this.refreshOneCell(row-1,column+1);
            this.partie.switchCell(row,column+1);
            this.refreshOneCell(row,column+1);
            this.partie.switchCell(row-1,column);
            this.refreshOneCell(row-1,column);

        } else if ( isRight(xTouch,yTouch,width,height) ) {
            this.partie.switchCell(row-1,column+1);
            this.refreshOneCell(row-1,column+1);
            this.partie.switchCell(row,column+1);
            this.refreshOneCell(row,column+1);
            this.partie.switchCell(row-1,column);
            this.refreshOneCell(row-1,column);
            this.partie.switchCell(row+1,column);
            this.refreshOneCell(row+1,column);
            this.partie.switchCell(row+1,column+1);
            this.refreshOneCell(row+1,column+1);

        } else if ( isBottomRight(xTouch,yTouch,width,height) ) {
            this.partie.switchCell(row+1,column+1);
            this.refreshOneCell(row+1,column+1);
            this.partie.switchCell(row,column+1);
            this.refreshOneCell(row,column+1);
            this.partie.switchCell(row+1,column);
            this.refreshOneCell(row+1,column);

        } else if ( isBottom(xTouch,yTouch,width,height) ) {
            this.partie.switchCell(row+1,column+1);
            this.refreshOneCell(row+1,column+1);
            this.partie.switchCell(row,column+1);
            this.refreshOneCell(row,column+1);
            this.partie.switchCell(row+1,column);
            this.refreshOneCell(row+1,column);
            this.partie.switchCell(row+1,column-1);
            this.refreshOneCell(row+1,column-1);
            this.partie.switchCell(row,column-1);
            this.refreshOneCell(row,column-1);

        } else if ( isBottomLeft(xTouch,yTouch,width,height) ) {
            this.partie.switchCell(row+1,column-1);
            this.refreshOneCell(row+1,column-1);
            this.partie.switchCell(row,column-1);
            this.refreshOneCell(row,column-1);
            this.partie.switchCell(row+1,column);
            this.refreshOneCell(row+1,column);

        } else if ( isLeft(xTouch,yTouch,width,height) ) {
            this.partie.switchCell(row+1,column-1);
            this.refreshOneCell(row+1,column-1);
            this.partie.switchCell(row,column-1);
            this.refreshOneCell(row,column-1);
            this.partie.switchCell(row+1,column);
            this.refreshOneCell(row+1,column);
            this.partie.switchCell(row-1,column-1);
            this.refreshOneCell(row-1,column-1);
            this.partie.switchCell(row-1,column);
            this.refreshOneCell(row-1,column);

        } else if ( isCenter(xTouch,yTouch,width,height) ) {
            this.partie.switchCell(row+1,column);
            this.refreshOneCell(row+1,column);
            this.partie.switchCell(row,column+1);
            this.refreshOneCell(row,column+1);
            this.partie.switchCell(row-1,column);
            this.refreshOneCell(row-1,column);
            this.partie.switchCell(row,column-1);
            this.refreshOneCell(row,column-1);
        }
    }


    /**
     * Method executed when the user make a touch (long click), switch the cell played and the
     * 8 cells next to it
     * @param row The row of the cell touched
     * @param column The column of the cell touched
     */
    public void cellTouched ( int row, int column, int xTouch, int yTouch, int width, int height ) {

        this.partie.switchCell(row,column);
        this.refreshOneCell(row,column);

        //above right
        this.partie.switchCell(row-1, column+1);
        this.refreshOneCell(row-1, column+1);

        //right
        this.partie.switchCell(row, column+1);
        this.refreshOneCell(row, column+1);

        //below right
        this.partie.switchCell(row+1, column+1);
        this.refreshOneCell(row+1, column+1);

        //bottom
        this.partie.switchCell(row+1, column);
        this.refreshOneCell(row+1, column);

        //below left
        this.partie.switchCell(row+1, column-1);
        this.refreshOneCell(row+1, column-1);

        //left
        this.partie.switchCell(row, column-1);
        this.refreshOneCell(row, column-1);

        //above left
        this.partie.switchCell(row-1, column-1);
        this.refreshOneCell(row-1, column-1);

        //above
        this.partie.switchCell(row-1, column);
        this.refreshOneCell(row-1, column);

        //check if game is won
        checkPartie();
    }



    /**
     * Method use to init the timer. A timer task will be scheduled to being executed after
     * 1 second delay, so 1 second after the user placing and keep his finger on a cell.
     * This methods require the row and the column of the cell to pass them to the reactive method 
     * together with the coordinates of the click and the width/height of the cell touched.
     * @param row The row of the cell
     * @param column The column of the cell
     * @param xTouch The horizontal coordinates 
     * @param yTouch The vertical coordinates
     * @param width The width of the cell touched
     * @param height The height of the cell touched
     */
    public void initTimer(int row, int column, int xTouch, int yTouch, int width, int height) {
        this.timer = new Timer();
        //create a task
        this.timer_task = new TouchTimerTask(row,column,this, xTouch, yTouch, width, height);
        //schedule the tack to be executed 0.5 secondes after this instruction
        this.timer.schedule(timer_task, 500);
        //inform the program that a task is scheduled
        this.timer_scheduled = true;
    }


    /**
     * Method use to stop the timer and remove it. It can cancelled a schedule task if this one has
     * not yet been executed.
     */
    public void stopTimer() {
        //if a timer exist, and a task is scheduled
        if ( this.timer != null && timer_scheduled ) {
            //cancel the scheduled task and remove the timer
            this.timer.cancel();
            this.timer = null;
            //inform the program that no task are scheduled
            timer_scheduled = false;
        }
    }


    /**
     * Get all ImageView in the layout and store it in the returned array
     * @return The ImageView array of the layout
     */
    public ImageView[][] getCells () {
        ImageView[][] tab = new ImageView[this.partie.getSize()][this.partie.getSize()];
        //Row 0
        tab[0][0] = findViewById(R.id.cell_0_0);
        tab[0][1] = findViewById(R.id.cell_0_1);
        tab[0][2] = findViewById(R.id.cell_0_2);
        tab[0][3] = findViewById(R.id.cell_0_3);
        tab[0][4] = findViewById(R.id.cell_0_4);
        tab[0][5] = findViewById(R.id.cell_0_5);

        //Row 1
        tab[1][0] = findViewById(R.id.cell_1_0);
        tab[1][1] = findViewById(R.id.cell_1_1);
        tab[1][2] = findViewById(R.id.cell_1_2);
        tab[1][3] = findViewById(R.id.cell_1_3);
        tab[1][4] = findViewById(R.id.cell_1_4);
        tab[1][5] = findViewById(R.id.cell_1_5);

        //Row 2
        tab[2][0] = findViewById(R.id.cell_2_0);
        tab[2][1] = findViewById(R.id.cell_2_1);
        tab[2][2] = findViewById(R.id.cell_2_2);
        tab[2][3] = findViewById(R.id.cell_2_3);
        tab[2][4] = findViewById(R.id.cell_2_4);
        tab[2][5] = findViewById(R.id.cell_2_5);

        //Row 3
        tab[3][0] = findViewById(R.id.cell_3_0);
        tab[3][1] = findViewById(R.id.cell_3_1);
        tab[3][2] = findViewById(R.id.cell_3_2);
        tab[3][3] = findViewById(R.id.cell_3_3);
        tab[3][4] = findViewById(R.id.cell_3_4);
        tab[3][5] = findViewById(R.id.cell_3_5);

        //Row 4
        tab[4][0] = findViewById(R.id.cell_4_0);
        tab[4][1] = findViewById(R.id.cell_4_1);
        tab[4][2] = findViewById(R.id.cell_4_2);
        tab[4][3] = findViewById(R.id.cell_4_3);
        tab[4][4] = findViewById(R.id.cell_4_4);
        tab[4][5] = findViewById(R.id.cell_4_5);

        //Row 5
        tab[5][0] = findViewById(R.id.cell_5_0);
        tab[5][1] = findViewById(R.id.cell_5_1);
        tab[5][2] = findViewById(R.id.cell_5_2);
        tab[5][3] = findViewById(R.id.cell_5_3);
        tab[5][4] = findViewById(R.id.cell_5_4);
        tab[5][5] = findViewById(R.id.cell_5_5);

        //if the board size is over 6
        if ( this.partie.getSize() >= 7 ) {
            //column 6
            tab[0][6] = findViewById(R.id.cell_0_6);
            tab[1][6] = findViewById(R.id.cell_1_6);
            tab[2][6] = findViewById(R.id.cell_2_6);
            tab[3][6] = findViewById(R.id.cell_3_6);
            tab[4][6] = findViewById(R.id.cell_4_6);
            tab[5][6] = findViewById(R.id.cell_5_6);
            //Row 6
            tab[6][0] = findViewById(R.id.cell_6_0);
            tab[6][1] = findViewById(R.id.cell_6_1);
            tab[6][2] = findViewById(R.id.cell_6_2);
            tab[6][3] = findViewById(R.id.cell_6_3);
            tab[6][4] = findViewById(R.id.cell_6_4);
            tab[6][5] = findViewById(R.id.cell_6_5);
            tab[6][6] = findViewById(R.id.cell_6_6);
        }

        //if the board size i over 8
        if ( this.partie.getSize() >= 8 ) {
            //column 7
            tab[0][7] = findViewById(R.id.cell_0_7);
            tab[1][7] = findViewById(R.id.cell_1_7);
            tab[2][7] = findViewById(R.id.cell_2_7);
            tab[3][7] = findViewById(R.id.cell_3_7);
            tab[4][7] = findViewById(R.id.cell_4_7);
            tab[5][7] = findViewById(R.id.cell_5_7);
            tab[6][7] = findViewById(R.id.cell_6_7);
            //Row 7
            tab[7][0] = findViewById(R.id.cell_7_0);
            tab[7][1] = findViewById(R.id.cell_7_1);
            tab[7][2] = findViewById(R.id.cell_7_2);
            tab[7][3] = findViewById(R.id.cell_7_3);
            tab[7][4] = findViewById(R.id.cell_7_4);
            tab[7][5] = findViewById(R.id.cell_7_5);
            tab[7][6] = findViewById(R.id.cell_7_6);
            tab[7][7] = findViewById(R.id.cell_7_7);
        }
        return tab;
    }


    /**
     * Method will update the image displayed on each cell. For each cell of the board saved in
     * class game, the cell in the layout which represent this display the appropriate image
     * according to the value of the boolean.
     */
    public void refrechCells () {
        boolean[][] game_grid = this.partie.getBoard();
        ImageView[][] grid_img = this.getCells();

        for ( int row = 0 ; row < this.partie.getSize() ; row ++ ) {
            for ( int column = 0 ; column < this.partie.getSize() ; column ++ ) {
                //if cell is activate
                if ( game_grid[row][column] ) {
                    //set activated image
                    grid_img[row][column].setImageResource(R.drawable.floor_active_switch_model);

                //else cell is inactive
                }else{
                    //set inactive image
                    grid_img[row][column].setImageResource(R.drawable.floor_switch_model);
                }
            }
        }
    }


    /**
     * Method will get the cell in the layout determinated by the coordinates passed in parameters,
     * and according the value of this cell (stored in the class game), defines the
     * corresponding image.
     * @param row The row of the cell
     * @param column The column of the cell
     */
    public void refreshOneCell (int row, int column) {
        if (row >= 0 && row < this.partie.getBoard().length && column >= 0 && column < this.partie.getBoard()[row].length) {
            //if cell is activated
            if (this.partie.getCell(row, column)) {
                //set activated image
                this.getCells()[row][column].setImageResource(R.drawable.floor_active_switch_model);
            
            //else cell is inactive
            }else{
                //set inactive image
                this.getCells()[row][column].setImageResource(R.drawable.floor_switch_model);
            }
        }
    }


    /**
     * Method will display the number of stroke saved in Game class after the string resource
     * stroke_label_text.
     */
    public void setStrokeLabel() {
        TextView txt = findViewById(R.id.stroke_label);
        txt.setText(getResources().getString(R.string.stroke_label_text) + " " + this.partie.getStroke());
    }


    /**
     * Methods that requires the coordinates of the click and the dimension of the cell touched.
     * Depending on this value, these methods return true if the click occured in the section of
     * the cell dedicate by the functions
     */
    public static boolean isAboveLeft ( int xTouch, int yTouch, int width, int height ) {
        boolean ret = false;
        int square_width = (int) width/3;
        int square_height = (int) height/3;
        if ( yTouch < square_height && xTouch < square_width ) {
            ret = true;
        }
        return ret;
    }
    public static boolean isAbove ( int xTouch, int yTouch, int width, int height ) {
        boolean ret = false;
        int square_width = (int) width/3;
        int square_height = (int) height/3;
        if ( yTouch < square_height && xTouch > square_width && xTouch < square_width*2 ) {
            ret = true;
        }
        return ret;
    }
    public static boolean isAboveRight ( int xTouch, int yTouch, int width, int height ) {
        boolean ret = false;
        int square_width = (int) width/3;
        int square_height = (int) height/3;
        if ( yTouch < square_height && xTouch > square_width*2 ) {
            ret = true;
        }
        return ret;
    }
    public static boolean isRight ( int xTouch, int yTouch, int width, int height ) {
        boolean ret = false;
        int square_width = (int) width/3;
        int square_height = (int) height/3;
        if ( yTouch > square_height && yTouch < square_height*2 && xTouch > square_width*2 ) {
            ret = true;
        }
        return ret;
    }
    public static boolean isBottomRight ( int xTouch, int yTouch, int width, int height ) {
        boolean ret = false;
        int square_width = (int) width/3;
        int square_height = (int) height/3;
        if ( yTouch > square_height*2 && xTouch > square_width*2 ) {
            ret = true;
        }
        return ret;
    }
    public static boolean isBottom ( int xTouch, int yTouch, int width, int height ) {
        boolean ret = false;
        int square_width = (int) width/3;
        int square_height = (int) height/3;
        if ( yTouch > square_height*2 && xTouch > square_width && xTouch < square_width*2 ) {
            ret = true;
        }
        return ret;
    }
    public static boolean isBottomLeft ( int xTouch, int yTouch, int width, int height ) {
        boolean ret = false;
        int square_width = (int) width/3;
        int square_height = (int) height/3;
        if ( yTouch > square_height*2 && xTouch < square_width ) {
            ret = true;
        }
        return ret;
    }
    public static boolean isLeft ( int xTouch, int yTouch, int width, int height ) {
        boolean ret = false;
        int square_width = (int) width/3;
        int square_height = (int) height/3;
        if ( yTouch > square_height && yTouch < square_height*2 && xTouch < square_width ) {
            ret = true;
        }
        return ret;
    }
    public static boolean isCenter ( int xTouch, int yTouch, int width, int height ) {
        boolean ret = false;
        int square_width = (int) width/3;
        int square_height = (int) height/3;
        if ( yTouch > square_height && yTouch < square_height*2 && xTouch > square_width && xTouch < square_width*2 ) {
            ret = true;
        }
        return ret;
    }


    /**
     * Debug method on the zone touched of a cell by a click. The parameters of the methods are
     * printed as well as a graph which show which area on a cell are touched.
     */
    public void debugTouchZone ( int xTouch, int yTouch, int width, int height ) {
        //xTouch represent column pixel, yTouch represent row pixel
        System.out.println("--> xTouch:"+xTouch+" yTouch:"+yTouch+" width:"+width+" height:"+height);
        int square_width = (int) width/3;
        int square_height = (int) height/3;
        //display the pixels of the boundaries of the areas of a cell
        System.out.println("--> Width: /3-"+square_width+" *2-"+square_width*2);
        System.out.println("--> Height: /3-"+square_height+" *2-"+square_height*2);

        String tab = "";
        //create the graph in string format
        if ( isAboveLeft(xTouch,yTouch,width,height) ) {
            tab += "X|";
        }else{
            tab += "O|";
        }
        if ( isAbove(xTouch,yTouch,width,height) ) {
            tab += "X|";
        }else{
            tab += "O|";
        }
        if ( isAboveRight(xTouch,yTouch,width,height) ) {
            tab += "X\n";
        }else{
            tab += "O\n";
        }
        if ( isLeft(xTouch,yTouch,width,height) ) {
            tab += "X|";
        }else{
            tab += "O|";
        }
        if ( isCenter(xTouch,yTouch,width,height) ) {
            tab += "X|";
        }else{
            tab += "O|";
        }
        if ( isRight(xTouch,yTouch,width,height) ) {
            tab += "X\n";
        }else{
            tab += "O\n";
        }
        if ( isBottomLeft(xTouch,yTouch,width,height) ) {
            tab += "X|";
        }else{
            tab += "O|";
        }
        if ( isBottom(xTouch,yTouch,width,height) ) {
            tab += "X|";
        }else{
            tab += "O|";
        }
        if ( isBottomRight(xTouch,yTouch,width,height) ) {
            tab += "X";
        }else {
            tab += "O";
        }
        System.out.println("Zone:\n"+tab);


    }




}
