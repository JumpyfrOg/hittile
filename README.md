
# **Hit Tile**

![forthebadge](https://forthebadge.com/images/badges/built-for-android.svg)
![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)
<br></br>
Hit Tile done on my spare time, again yes. It's a simple android game.
<br></br>
<br></br>

# The Game
The goal is simple, you just have to activate all the button on the board. Luckily some button are already activated. Try to make your best by complete the grid with as few strokes as possible. Here in the image below, is a 6 by 6 grid, you can choose from the menu a game board of 6, 7 or 8 length. You can also return your game board to its original appearance, if your board is harder than it was after you manipulated it. But is not easy like you think thank to the touch logic of the game.
<br></br>
<img src="https://gitlab.com/JumpyfrOg/hittile/-/raw/master/img_readme/board_in_game.jpg"  width="400" height="700">

<br></br>
<br></br>
# Touch logic

The touch logic of the game is not by default. When you make a click, a area around the click is generated (not visible in 2.0 version and older), and all the cells in this area are touched. So you must think to find the best plays to complete quicly the board, the good cell to play and the good position for clicking on it.
You have two type of play:

* First, the click, a quick pressure on a cell generate a small area.
![Area of a click](https://gitlab.com/JumpyfrOg/hittile/-/raw/master/img_readme/click_cells_touched.png)*Red dot is the position of the click, the black circle the area of the click, and the red cells, the cells that will be activated (cells touched)*
<br></br>
<br></br>
* Second, the touch, a long pressure on a cell generate a great area. (Currently with the version 2.0, the position of a touch on a cell is not concerned, it is always considered to be the center of the cell)
![Area of a touch](https://gitlab.com/JumpyfrOg/hittile/-/raw/master/img_readme/touch_cells_touched.png)
<br></br>
*Red dot is the position of the click, the black circle the area of the click, and the red cells, the cells that will be activated (cells touched)*

<br></br>
# Installation and start
To run the application, you need to download the file "[*hit_tile.apk*](https://gitlab.com/JumpyfrOg/hittile/-/blob/master/hit_tile.apk)" at the root of the project. Then executed it on a smartphone with the operating system android and its done.

<br></br>
# Service used
- Online photo editor : [Pixlr](https://pixlr.com/)
- IDE : [Android Studio](https://developer.android.com/studio)
- Online markdown editor : [Stackedit](https://stackedit.io/)
- Switch model from "*Zelda : Phantom hourglass*" : [Switch](https://zelda.gamepedia.com/File:PH_Floor_Switch_Model.png) / [Active switch](https://zelda.gamepedia.com/File:PH_Floor_Switch_Active_Model.png)

<br></br>
# Contact
- Mail : mcharron29@gmail.com
- Twitter : [Charron M](https://twitter.com/CharronM5)
